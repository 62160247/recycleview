package com.jarupong.affirmations

import android.content.Context
import com.jarupong.affirmations.adapter.ItemAdapter
import com.jarupong.affirmations.model.Affirmation
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.manipulation.Ordering
import org.mockito.Mockito.mock

class AffirmationsAdapterTests {
    private val context = mock(Context::class.java)

    @Test
    fun adaptor_size() {
        val data = listOf(
            Affirmation(R.string.affirmation1, R.drawable.image1),
            Affirmation(R.string.affirmation2, R.drawable.image2)
        )
        val adapter = ItemAdapter(context, data)
        assertEquals("ItemAdapter is not the correct size", data.size, adapter.itemCount)
    }
}